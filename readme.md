# ExportGameData
Godot plugin to automatically export the folder `res://game_data` next to executable.
Plugin also prevents it from being inclued inside package file or executable.


## Installation
1. Copy `addons/export_game_data` folder to your project (must be inside `addons`)
2. Enable the plugin in the editor (name: `ExportGameData`)
3. Make a new folder `res://game_data`


## Usage
Simply place any files you wish to be next to executable in `res://game_data`

This plugin also has helper functions:

1) `ExportGameData.game_data_path()`
   <br/>
   If the game is ran in the editor, this function will return a full
   [globalized path](https://docs.godotengine.org/en/stable/classes/class_projectsettings.html#class-projectsettings-method-globalize-path)
   to `res://game_data`.
   <br/><br/>
   Example:
   ```godot
   ExportGameData.game_data_path()
   # => /home/username/workspace/project-name/game_data
   # or
   # => /path/to/game/folder
   ```

1) `ExportGameData.game_data_item_path(item_path: String)`
   <br/>
   If the game is ran in the editor, this function will return a full
   [globalized path](https://docs.godotengine.org/en/stable/classes/class_projectsettings.html#class-projectsettings-method-globalize-path)
   to `res://game_data` + `item_path`.
   <br/><br/>
   Example:
   ```godot
   ExportGameData.game_data_item_path("test.json")
   # => /home/username/workspace/project-name/game_data/test.json
   # or
   # => /path/to/game/folder/test.json
   ```


## Usage (.Net)
Plugin works fine on mono .Net version.
<br/>
To use built in functions in C#, it can be done like so:

1) `ExportGameData.game_data_path()`
```c#
using Godot;

GDScript      EGDScript = (GDScript)GD.Load("res://addons/export_game_data/export_game_data.gd");
Godot.Variant variant   = EGDScript.Call("game_data_path");
String        res       = variant.ToString();
```

1) `ExportGameData.game_data_item_path(item_path: String)`
```c#
using Godot;

String        filePath  = "some/path/in/res";
GDScript      EGDScript = (GDScript)GD.Load("res://addons/export_game_data/export_game_data.gd");
Godot.Variant variant   = EGDScript.Call("game_data_item_path", filePath);
String        res       = variant.ToString();
```


## License
ExportGameData © 2023 by
[EdCordata](https://github.com/EdCordata)
is licensed under
[CC BY-SA 4.0 ](https://creativecommons.org/licenses/by-sa/4.0)