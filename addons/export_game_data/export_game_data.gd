class_name ExportGameData extends Node

static func game_data_path() -> String:
	if OS.has_feature("editor"):
		return ProjectSettings.globalize_path( ProjectSettings.globalize_path("res://game_data") )
	else:
		return ProjectSettings.globalize_path( OS.get_executable_path().get_base_dir() )

static func game_data_item_path(item_path: String) -> String:
	return ProjectSettings.globalize_path(game_data_path() + '/' + item_path)
