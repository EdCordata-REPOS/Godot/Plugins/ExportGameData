@tool
extends EditorPlugin

const ExportGameData:       Script = preload("res://addons/export_game_data/export_game_data.gd")
const ExportGameDataPlugin: Script = preload("res://addons/export_game_data/export_game_data_plugin.gd")

var export_game_data_plugin: ExportGameDataPlugin = ExportGameDataPlugin.new()

func _enter_tree():
  add_export_plugin(export_game_data_plugin)

func _exit_tree():
  remove_export_plugin(export_game_data_plugin)
  export_game_data_plugin = null
