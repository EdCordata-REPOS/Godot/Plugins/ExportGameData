@tool
class_name ExportGameDataPlugin extends EditorExportPlugin

var game_data_files:  Array = []
var output_root_dir:  String
var project_root_dir: String

func _export_begin(features: PackedStringArray, is_debug: bool, path: String, flags: int) -> void:
	output_root_dir  = path.get_base_dir() + "/"
	project_root_dir = ProjectSettings.globalize_path("res://")

func _export_file(path: String, type: String, features: PackedStringArray) -> void:
	if path.begins_with("res://game_data/"):
		_copy_to_executable_path(path)
		skip()

func _copy_to_executable_path(res_source_file_path: String) -> void:
	var relative_file_path: String = res_source_file_path.substr(16); # remove "res://game_data/"
	var source_file_path:   String = project_root_dir + "game_data/" + relative_file_path
	var target_file_path:   String = output_root_dir  + relative_file_path
	var target_dir_path:    String = target_file_path.get_base_dir()

	DirAccess.make_dir_recursive_absolute(target_dir_path)
	DirAccess.copy_absolute(source_file_path, target_file_path)
